(function (selector) {
  document.querySelector('body').style = "background: linear-gradient(340.98deg, #FC9D00 -21.76%, #FBB43E 45.52%, #FDE63E 88.75%);"
  let isSet = false
  document.querySelector('.poweredby')?.remove()
  let currentElement = document.querySelector(selector)
  const wrapper = currentElement
  while (currentElement !== document.body) {
    const parent = currentElement.parentNode;
    let idx = 0;
    while (parent.children.length > 1) {
      const element = parent.children[idx];
      if (currentElement !== element) {
        parent.removeChild(element)
      } else {
        idx = 1;
      }
    }
    currentElement = parent
  }
  wrapper.style = 'height: 100vh; display: flex; justify-content: center; align-items: center'
})('.editor-wrapper')