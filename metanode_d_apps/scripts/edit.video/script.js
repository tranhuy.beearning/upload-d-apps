(function (selector) {
  document.querySelector('body').style = "background: linear-gradient(321.39deg, #4628FF -0.56%, #6951FF -0.54%, #CBE6FF 100.46%);"
  let isSet = false
  // const setBg = setInterval(() => {
  //   document.querySelector('.pintura-editor > :first-child').style = "background: #bebebe"
  //   isSet = true
  //   clearInterval(setBg)
  // }, 100)
  document.querySelector('.poweredby')?.remove()
  let currentElement = document.querySelector(selector)
  const wrapper = currentElement
  while (currentElement !== document.body) {
    const parent = currentElement.parentNode;
    let idx = 0;
    while (parent.children.length > 1) {
      const element = parent.children[idx];
      if (currentElement !== element) {
        parent.removeChild(element)
      } else {
        idx = 1;
      }
    }
    currentElement = parent
  }
  wrapper.style = 'height: 100vh; display: flex; justify-content: center; align-items: center'
})('.editor-wrapper')