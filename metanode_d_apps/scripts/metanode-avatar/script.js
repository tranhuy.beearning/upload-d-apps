// delete header script
// skip the email popup when done creating avatar
setCookie('rpm-skipsignup', true, 200);
let fired = false;
(() => {

  setInterval(() => {
    document.querySelector('[class^=TopButtons_dropDownButton]')?.remove()
    document.querySelector('[class^="LanguageSelector"]')?.remove()
  }, 200)
})();

// delete player ready me sigin button
(() => {
  setInterval(() => {
    const ssoBtn = document.querySelector('#sso-btn')
    ssoBtn?.parentNode?.remove()
  }, 200)
})();

(() => {
  setInterval(() => {
    // tos stands for Term of Service
    const deleteToS = () => {
      const intervalDelete = setInterval(() => {
        const tos = document.querySelector('[class^="Policies_confirmation"]')
        tos?.remove()
        clearInterval(intervalDelete)
      }, 200)
    }
    const maleBtn = document.querySelector('#male-button')
    const femaleBtn = document.querySelector('#female-button')
    const nonbiBtn = document.querySelector('#nonbi-button')
    maleBtn?.addEventListener('click', deleteToS)
    femaleBtn?.addEventListener('click', deleteToS)
    nonbiBtn?.addEventListener('click', deleteToS)
  }, 200)
})();

(() => {
  setInterval(() => {
    const modal = document.querySelector('#bake-modal')
    const belowInfo = modal?.querySelector(':scope > div > :nth-child(2)')
    const imgWrapper = modal?.querySelector(':scope > div > :first-child')
    const linkContainer = modal?.querySelector(':scope > div > :first-child')
    const srcLink = linkContainer?.querySelector(':scope img')?.getAttribute('src')
    const link = srcLink?.split('.png')[0] + '.png'
    if (belowInfo?.childElementCount > 1) {
      belowInfo?.querySelector(':scope > :first-child')?.remove()
    }
    document.querySelector('.copy-area')?.remove()
    const download = document.querySelector('h3.text-modal-primary');
    download.style.color = 'black'
    download.style.background = '#00ffff'
    download.style.width = 'fit-content'
    download.style.margin = '10px auto'
    download.style.padding = '5px'
    download.style.borderRadius = '10px'
    download.style.padding = '5px 15px'
    const downloadBtn = belowInfo?.querySelector(':scope > h3')
    function handleDownload() {
      download.innerHTML = `<style>
        .loader {
          border: 4px solid #fff;
          border-radius: 50%;
          border-top: 4px solid #3498db;
          width: 2rem;
          height: 2rem;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
        </style>
        <div class="loader"></div>
      `
      downloadImage(link).then(res => {
        download.innerHTML = "Download the avatar"
        fired = false
      })
      fired = true
    }
    downloadBtn?.addEventListener('click', handleDownload, { once: true })
  }, 200)
})()

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function downloadImage(url) {
  if (fired) return
  return fetch(url)
    .then(res => res.blob())
    .then(blob => {
      let blobUrl = window.URL.createObjectURL(blob);
      let a = document.createElement('a');
      a.download = url.replace(/^.*[\\\/]/, '');
      a.href = blobUrl;
      a.click();
    })
}