(function deleteDomMsn() {
  if (window.location.pathname === "/vi-vn") {
    if (
      window.matchMedia("(max-height: 250px) and (max-width: 280px)").matches
    ) {
      (function script2x2And2x4() {
        let queryEl = document.querySelector(
          "div#maincontent div.todaystripe div.blackbackground"
        );
        queryEl.style.margin = "0";
        while (queryEl !== document.body) {
          const parent = queryEl.parentNode;
          let idx = 0;
          while (parent.children.length > 1) {
            const element = parent.children[idx];
            if (queryEl !== element) {
              parent.removeChild(element);
            } else {
              idx = 1;
            }
          }
          queryEl = parent;
        }
      })();
      document.querySelector("body").style.overflow = "hidden";

      const removeStyle = document.querySelector("div#maincontent");
      removeStyle.style.margin = "0";
      removeStyle.style.height = "auto";
      removeStyle.style.background = "transparent";

      const customStyle = document.querySelector("div.ip");
      customStyle.style.width = "280px";
      customStyle.style.position = "relative";
      customStyle.style.top = "-40px";

      const customCaption = document.querySelectorAll(".caption");
      const deleteSlide = document.querySelectorAll(
        ".caption > div.metadata > span.slideindex"
      );

      if (
        window.matchMedia("(max-height: 160px) and (max-width: 160px)").matches
      ) {
        document.querySelector("div.ip > button.leftarrow").remove();
        document.querySelector("div.ip > button.rightarrow").remove();
        const customSlide = document.querySelectorAll(
          "div.ip > ul > li > a > img"
        );

        for (var i = 0; i < customSlide.length; i++) {
          customSlide[i].style.width = "180px";
        }
        for (var i = 0; i < deleteSlide.length; i++) {
          deleteSlide[i].remove();
        }
        for (var i = 0; i < customCaption.length; i++) {
          customCaption[i].style.margin = "0 0 0 40px";
          customCaption[i].style.width = "46%";
        }
      } else {
        for (var i = 0; i < customCaption.length; i++) {
          customCaption[i].style.margin = "0 0 0 40px";
          customCaption[i].style.width = "88%";
        }
      }

      const customHeightTitle = document.querySelectorAll(".caption > p.title");
      for (var i = 0; i < customHeightTitle.length; i++) {
        customHeightTitle[i].style.height = "4.5rem";
      }
    } else {
      try {
        (function script4x4() {
          let queryEl = document.querySelector("div#maincontent");
          queryEl.style.margin = "0";
          while (queryEl !== document.body) {
            const parent = queryEl.parentNode;
            let idx = 0;
            while (parent.children.length > 1) {
              const element = parent.children[idx];
              if (queryEl !== element) {
                parent.removeChild(element);
              } else {
                idx = 1;
              }
            }
            queryEl = parent;
          }
        })();
        const removeEl = document.querySelectorAll(".morefrom");
        for (let i = 0; i < removeEl.length; i++) {
          removeEl[i].remove();
        }
        const customCaption = document.querySelectorAll(".caption");

        for (var i = 0; i < customCaption.length; i++) {
          customCaption[i].style.margin = "0 0 0 40px";
          customCaption[i].style.width = "88%";
        }

        const customStyle = document.querySelector("div.ip");
        customStyle.style.width = "280px";
        // comment this in case of conflict
        const pageWrap = document.querySelector('.page-wrap')
        const header = document.createElement('header')
        header.id = 'header'
        header.setAttribute("style", "display: flex; align-items: center; justify-content: space-between; padding: 0.5rem 0.5rem; height: 64px; background: #000")
        header.innerHTML = `
        <div style="display: flex; gap: 0.5rem; align-items: center">
          <img src='https://gitlab.com/tranhuy.beearning/upload-d-apps/-/raw/main/metanode_d_apps/public/News.png' alt='logo' width='50' height ='auto' /><div style="display: inline-block; color: white; font-size: 24px">News</div>
        </div>
        <div style="display: flex; align-items: center; border-radius: 10px; background: #eee; padding: 1rem">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#000000" height="30px" width="30px" version="1.1" id="Capa_1" viewBox="0 0 198.715 198.715" xml:space="preserve">
            <g>
              <path d="M161.463,48.763c-2.929-2.929-7.677-2.929-10.607,0c-2.929,2.929-2.929,7.677,0,10.606   c13.763,13.763,21.342,32.062,21.342,51.526c0,19.463-7.579,37.761-21.342,51.523c-14.203,14.204-32.857,21.305-51.516,21.303   c-18.659-0.001-37.322-7.104-51.527-21.309c-28.405-28.405-28.402-74.625,0.005-103.032c2.929-2.929,2.929-7.678,0-10.606   c-2.929-2.929-7.677-2.929-10.607,0C2.956,83.029,2.953,138.766,37.206,173.019c17.132,17.132,39.632,25.697,62.135,25.696   c22.497-0.001,44.997-8.564,62.123-25.69c16.595-16.594,25.734-38.659,25.734-62.129C187.199,87.425,178.059,65.359,161.463,48.763   z"/>
              <path d="M99.332,97.164c4.143,0,7.5-3.358,7.5-7.5V7.5c0-4.142-3.357-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v82.164   C91.832,93.807,95.189,97.164,99.332,97.164z"/>
            </g>
          </svg>
        </div>
      `
        pageWrap.prepend(header)
        //
      } catch (e) {
        const content = document.querySelector('fluent-design-system-provider > entry-point-hp-mobile').shadowRoot.querySelector('.feed-container');
        const potentialHeader = document.querySelector('#root')
        const header = document.createElement('header')
        header.id = 'header'
        header.setAttribute("style", "display: flex; align-items: center; justify-content: space-between; padding: 0.5rem 0.5rem; height: 64px; background: #000")
        header.innerHTML = `
          <div style="display: flex; gap: 0.5rem; align-items: center">
            <img src='https://gitlab.com/tranhuy.beearning/upload-d-apps/-/raw/main/metanode_d_apps/public/News.png' alt='logo' width='50' height ='auto' /><div style="display: inline-block; color: white; font-size: 24px">News</div>
          </div>
          <div style="display: flex; align-items: center; border-radius: 10px; background: #eee; padding: 10px">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#000000" height="30px" width="30px" version="1.1" id="Capa_1" viewBox="0 0 198.715 198.715" xml:space="preserve">
              <g>
                <path d="M161.463,48.763c-2.929-2.929-7.677-2.929-10.607,0c-2.929,2.929-2.929,7.677,0,10.606   c13.763,13.763,21.342,32.062,21.342,51.526c0,19.463-7.579,37.761-21.342,51.523c-14.203,14.204-32.857,21.305-51.516,21.303   c-18.659-0.001-37.322-7.104-51.527-21.309c-28.405-28.405-28.402-74.625,0.005-103.032c2.929-2.929,2.929-7.678,0-10.606   c-2.929-2.929-7.677-2.929-10.607,0C2.956,83.029,2.953,138.766,37.206,173.019c17.132,17.132,39.632,25.697,62.135,25.696   c22.497-0.001,44.997-8.564,62.123-25.69c16.595-16.594,25.734-38.659,25.734-62.129C187.199,87.425,178.059,65.359,161.463,48.763   z"/>
                <path d="M99.332,97.164c4.143,0,7.5-3.358,7.5-7.5V7.5c0-4.142-3.357-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v82.164   C91.832,93.807,95.189,97.164,99.332,97.164z"/>
              </g>
            </svg>
          </div>
        `
        potentialHeader.prepend(header);
        (function (queryEl) {
          while (queryEl !== document.body) {
            const parent = queryEl.parentNode;
            let idx = 0;
            while (parent?.children?.length > 1) {
              const element = parent?.children[idx];
              if (queryEl !== element) {
                parent.removeChild(element);
              } else {
                idx = 1;
              }
            }
            queryEl = parent;
          }
        })(content);
      }
    }
  } else {
    setTimeout(() => {
      (function scriptDetail() {
        // translate logo
        const translateDom = document
          .querySelector("mobile-header")
          .shadowRoot.querySelector(
            ".mobile-header-container > msn-mobile-header"
          )
          .shadowRoot.querySelector(
            "fluent-design-system-provider > div > div > div.logo > a"
          );
        translateDom.style.marginBottom = '12px'
        translateDom.innerHTML =
          `<img src='https://gitlab.com/tranhuy.beearning/upload-d-apps/-/raw/main/metanode_d_apps/public/News.png' alt='logo' width='50' height ='auto' /><div style="display: inline-block">News</div>`; //translate logo hear
        // delete dom header
        document
          .querySelector("mobile-header")
          .shadowRoot.querySelector(
            ".mobile-header-container > msn-mobile-header"
          )
          .shadowRoot.querySelector(
            "fluent-design-system-provider > div > div > div.user-pref-container"
          ).style.display = "none";

        // delete share
        document
          .querySelector(
            ".viewsHeaderWrapper > div > div > div > div > div > div > div > div > div > div > div"
          )
          .remove();

        // delete reading app
        document
          .querySelector(
            ".articlePage_articleContentContainer-DS-EntryPoint1-1 > div > div > div > div >div >section > div#continuousReadingContainer > div > button"
          )
          .remove();
        // delete footer
        document.querySelector("mobile-footer").remove();
        // delete dom ad
        let queryElContent = document.querySelectorAll(
          ".articlePage_articleContentContainer-DS-EntryPoint1-1 > div > div > div > div > div > section.articlePageBodySection > article > div > .intra-article-module"
        );

        for (let i = 0; i < queryElContent.length; i++) {
          queryElContent[i].remove();
        }

        let timer = setInterval(() => {
          let queryEl = document
            .querySelector(
              ".articlePage_riverContainer-DS-EntryPoint1-1 > div > desktop-feed-views"
            )
            .shadowRoot.querySelectorAll(
              "fluent-design-system-provider > .desktop-feed > div > msft-feed-layout"
            )


          for (let i = 0; i < queryEl.length; i++) {
            queryEl[i].remove();
          }
        }, 3000)
        setTimeout(() => { clearInterval(timer) }, 12000)

        document
          .querySelector(
            ".articlePage_riverContainer-DS-EntryPoint1-1 > div > desktop-feed-views"
          )
          .shadowRoot.querySelector(
            "fluent-design-system-provider > .desktop-feed > div > msft-feed-layout"
          ).style.display = "block";
      })();
    })
  }
})();
