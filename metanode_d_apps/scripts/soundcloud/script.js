//final
(function () {
  let query;
  let mobile = window.matchMedia('screen and (min-width: 366px)');
  let widget = window.matchMedia(
    'screen and (min-width: 140px) and (max-width: 365px) and (min-height: 140px) and (max-height: 365px)',
  );
  let widget2x2 = window.matchMedia(
    'screen and (min-width: 140px) and (max-width: 170px) and (min-height: 140px) and (max-height: 170px)',
  );
  let widget4x2 = window.matchMedia(
    'screen and (min-width: 280px) and (max-width: 365px) and (min-height: 140px) and (max-height: 170px)',
  );

  if (mobile.matches) {
    function notLogin() {
      document
        .querySelector('[class^="Header_RightContainer"] > :nth-child(2)')
        ?.remove();
      document.querySelector('h1')?.nextElementSibling?.remove();
      document.querySelector('div[class^="Tooltip"]')?.remove();
      document.body.style.overflow = 'auto';
    }
    function logined() {
      document.querySelector('div[data-testid="modal-overlay"]')?.remove();
      document.querySelector('div[class^=Header_RightContainer] button')?.remove();
    }

    setInterval(() => {
      //add exit btn
      let myImg = document.createElement('img');
      myImg.setAttribute(
        'src',
        'https://www.svgrepo.com/show/194416/power-button-power-on.svg',
      );
      myImg.setAttribute('alt', 'logo');
      myImg.style.width = '32px';
      myImg.style.height = '32px';
      myImg.style.zIndex = 100;
      myImg.style.borderRadius = '100%';
      myImg.style.background = 'white';
      let text = document.createElement('div');
      text.innerText = 'Exit App';
      text.style.fontSize = '10px';
      text.style.color = '#666';
      text.style.margin = '4px 0';

      let download = document.querySelector('a[href="/download"]');
      if (download) {
        let parent = download.parentNode;
        if (parent) {
          parent.appendChild(text);
          parent.style.display = 'flex';
          parent.style.flexDirection = 'column';
          parent.style.justifyContent = 'center';
          parent.style.alignItems = 'center';
        }
        download.replaceWith(myImg);
      }
      document.querySelector('a[data-testid="open-in-store-link"]')?.remove();
      document.querySelector('footer')?.remove();
      let isLogin = document.querySelector('button[data-testid="sign-in-button"]');
      isLogin ? notLogin() : logined();
    }, 500);
  }
  if (widget.matches) {
    document.querySelector('.l-product-banners')?.remove();
    document.querySelector('header')?.remove();
    document.querySelector('.playControls')?.remove();
    document.querySelector('div[class^="banner"]')?.remove();

    window.onscroll = () => window.scrollTo(0, 0);

    query = document.querySelector('.l-container');
    if (query !== null) query.style.paddingTop = 0;

    query = document.querySelector('.soundTitle__title');
    if (query !== null) {
      query.style.fontSize = '10px';
      query.parentNode.parentNode.style.maxWidth = '70vw';
      query.children[0].style.lineHeight = '32px';
    }

    query = document.querySelector('div[class*="foreground"]');
    if (query !== null) {
      query.style.paddingLeft = 0;
      query.style.paddingTop = '30vh';
      if (widget4x2.matches) {
        query.style.paddingTop = '10vh';
      }
    }

    query = document.querySelector('div[class*="playButton"]');
    if (query !== null) {
      query.style.height = '20vh';
      query.style.width = '20vw';
      query.style.marginLeft = '5px';
      if (widget4x2.matches) {
        query.style.height = '40vh';
      }
    }

    query = document.querySelector('.soundTitle__title');
    if (query !== null) {
      console.log('go into title query');
      query.parentNode.style.display = 'flex';
      query.style.backgroundColor = 'transparent';
      query.style.padding = 0;
      query = query.children[0].style;
      query.fontSize = '20px';
      query.lineHeight = '140%';
      query.width = '100%';
      query.display = '-webkit-box';
      query['-webkit-line-clamp'] = 3;
      query['-webkit-box-orient'] = 'vertical';
      query['overflow'] = 'hidden';

      if (widget2x2.matches) {
        query.fontSize = '10px';
      }
    }

    query = document.querySelector('a[class="sc-link-secondary"]');
    if (query !== null) {
      if (widget2x2.matches) {
        query.style.fontSize = '10px';
      }
    }
  }
  setInterval(() => {
    document.querySelector('#onetrust-accept-btn-handler')?.click();
  }, 500);
})();
