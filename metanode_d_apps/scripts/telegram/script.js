(function () {
  let mobile = window.matchMedia('screen and (min-width: 366px) ');
  let widget = window.matchMedia(
    'screen and (min-width: 140px) and (max-width: 365px) and (min-height: 140px) and (max-height: 365px)',
  );
  let widget2x2 = window.matchMedia(
    'screen and (min-width: 140px) and (max-width: 170px) and (min-height: 140px) and (max-height: 170px)',
  );
  if (mobile.matches) {
    if (document.querySelector('div[class*="Auth"]')) {
      let observe = new MutationObserver(direct);
      let observeLoading = new MutationObserver(loadingQr);
      function direct(mutations) {
        for (let mutation of mutations) {
          if (mutation.type === 'attributes') {
            let myBtn = document.createElement('button');
            myBtn.innerText = 'EXIT';
            myBtn.style.color = 'rgb(51,144,236)';
            myBtn.style.fontSize = '20px';
            myBtn.style.border = 'none';
            myBtn.style.background = 'white';
            myBtn.style.marginTop = '12px'
            myBtn.addEventListener('focus', () => {myBtn.style.outline = 'none'})
            document.querySelector('button')?.replaceWith(myBtn);
            observe.disconnect();
          }
        }
      }
      function loadingQr(mutations) {
        for (let mutation of mutations) {
          if (mutation.type === 'childList') {
            document.querySelector('button').click();
            observeLoading.disconnect();
          }
        }
      }
      observe.observe(document.querySelector('#UiLoader div div div'), {
        attributes: true,
      });
      observeLoading.observe(document.querySelector('.qr-outer'), {
        childList: true,
      });
    }
  } else if (widget.matches) {
    document.querySelector('#MiddleColumn')?.remove();
    document.querySelector('#RightColumn-wrapper')?.remove();
    document.querySelector('.LeftMainHeader')?.remove();
    document.querySelector("button[class='Button default primary round']")?.remove();
    let curElement = document.querySelector('#LeftColumn');
    if (curElement) {
      curElement.style.height = '100%';
      curElement.style.width = '100%';
      curElement.style.minWidth = '100%';
      curElement.style.maxWidth = '100%';
    }
    if (widget2x2.matches) {
      document
        .querySelectorAll('div[class^="Avatar"]')
        .forEach((item) => item.remove());
      document
        .querySelectorAll('div[class="LastMessageMeta"]')
        .forEach((item) => item.remove());
    }
  }
})();
