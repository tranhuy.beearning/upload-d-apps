(function () {
  let isLogin;
  let body = document.querySelector('body');
  let myImg = document.createElement('img');
  body.style.position = 'relative';

  myImg.setAttribute(
    'src',
    'https://www.svgrepo.com/show/194416/power-button-power-on.svg'
    );
  myImg.setAttribute('alt', 'logo');
  myImg.style.width = '32px';
  myImg.style.height = '32px';
  myImg.style.position = 'absolute';
  myImg.style.zIndex = 100;
  myImg.style.right = '20px';
  myImg.style.top = '20px';
  myImg.style.borderRadius = '100%'
  myImg.style.background = 'white'
  body.appendChild(myImg);

  setInterval(() => {
    isLogin = document.querySelector('a[data-e2e="profile-icon"]')?.href;
    document.querySelector('button[data-e2e="bottom-cta-cancel-btn"]')?.click();

    if (isLogin === 'https://www.tiktok.com/profile') {
      document
        .querySelector('div[class*="DivCenterWrapper"]')
        ?.parentNode.parentNode.remove();
      document.querySelector('div[class*="DivBottomWrapper "]')?.remove();
      document.querySelector('a[data-e2e="nav-following"]')?.remove();
      document.querySelector('footer[class*="FooterContainer"]')?.remove();
      document.querySelector('div[data-e2e="footer-guide"]')?.remove();
      document.querySelector('nav[class*="NavTab"')?.remove();
      document.querySelector('a[class*="StyledLinkSearch"')?.remove();
    } else {
      document.querySelector('div[data-focus-guard="true"]')?.parentNode.remove();
      document.querySelector('div[class*="DivBottomWrapper "]')?.remove();
      document.querySelector('a[data-e2e="nav-following"]')?.remove();
      document
        .querySelector('button[class*="ButtonOpenTT"')
        ?.parentNode.parentNode.remove();
      document.querySelector('div[class*="DivFloatButtonWrapper"]')?.remove();
      document.querySelector('nav[class*="NavTab"')?.remove();
      document.querySelector('a[class*="StyledLinkSearch"')?.remove();
      document.querySelector('div[data-e2e="open-titok-icon"]')?.remove();
      document.querySelector('div[data-e2e="footer-guide"]')?.remove();
    }
  }, 500);
})();
