// mobile
const botBtns = document.querySelector(".rightBottomButtonsContainer-E1_1")
botBtns.parentElement.removeChild(botBtns)
const news = document.querySelector('.newsContent-E1_1')
news.parentNode.removeChild(news)
const menu = document.querySelector('.mobileHeader-E1_1')
menu.parentNode.removeChild(menu);
document.querySelector('#weatherMiniMapContainer').remove();

// full screen mobile
(function (selector) {
  let currentElement = document.querySelector(selector)
  currentElement.parentNode.style.marginTop = 0
  while (currentElement !== document.body) {
    const parent = currentElement.parentNode;
    let idx = 0;
    while (parent.children.length > 1) {
      const element = parent.children[idx];
      if (currentElement !== element) {
        parent.removeChild(element)
      } else {
        idx = 1;
      }
    }
    currentElement = parent
  }
})('[class*="sectionContainer"]');

(() => {
  document.querySelector('[class^="locationNameContainer"]')?.remove()
  document.querySelector('[class*=weatherMapCarousel]')?.remove()
})()
if (matchMedia('(min-height: 400px)').matches) {
  const exit = document.createElement('div')
  exit.innerHTML = `<div style="position: absolute; z-index: 99999; top: 1rem; right: 1rem; display: flex; align-items: center; border-radius: 10px; background: rgba(255, 255, 255, 0.3); padding: 0.5rem">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#fff" height="24px" width="24px" version="1.1" id="Capa_1" viewBox="0 0 198.715 198.715" xml:space="preserve">
            <g>
              <path d="M161.463,48.763c-2.929-2.929-7.677-2.929-10.607,0c-2.929,2.929-2.929,7.677,0,10.606   c13.763,13.763,21.342,32.062,21.342,51.526c0,19.463-7.579,37.761-21.342,51.523c-14.203,14.204-32.857,21.305-51.516,21.303   c-18.659-0.001-37.322-7.104-51.527-21.309c-28.405-28.405-28.402-74.625,0.005-103.032c2.929-2.929,2.929-7.678,0-10.606   c-2.929-2.929-7.677-2.929-10.607,0C2.956,83.029,2.953,138.766,37.206,173.019c17.132,17.132,39.632,25.697,62.135,25.696   c22.497-0.001,44.997-8.564,62.123-25.69c16.595-16.594,25.734-38.659,25.734-62.129C187.199,87.425,178.059,65.359,161.463,48.763   z"/>
              <path d="M99.332,97.164c4.143,0,7.5-3.358,7.5-7.5V7.5c0-4.142-3.357-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v82.164   C91.832,93.807,95.189,97.164,99.332,97.164z"/>
            </g>
          </svg>
        </div>`
  document.querySelector('#root').prepend(exit)
}
// widgets
// remove layouts
const widgets = matchMedia('(max-width: 364px) and (max-height: 382px)')
if (widgets.matches) {
  // document.querySelector('[class^="baseHeader"]')?.remove()
  // document.querySelector('[class^="locationSearch"]')?.parentNode?.remove()
  // // document.querySelector('[class^="backgroundContainer"]').remove()
  // // remove fixed components
  // const removeFixedCompo = setInterval(() => {

  //   document.querySelector('[class^="customizeModal"]')?.remove()
  //   document.querySelector('#eplantEntranceBubble')?.remove()
  //   document.querySelector('[class*="feedback_link_default"]')?.remove()
  //   document.querySelector('footer').remove()
  //   document.querySelector('[class^="rightBottomButtonsContainer"]').remove()
  //   clearInterval(removeFixedCompo)
  // }, 200)
  // // const observer = new MutationObserver(removeFixedCompo)
  // // observer.observe(document.querySelector('#root'), { subtree: true, childList: true, characterData: true })

  // const content = document.querySelector('[class^="baseHeader_spacer"]').nextElementSibling
  // content.style.marginTop = 0
  // document.querySelector('[class^="locationNameContainer"]').remove()
  // const style = document.createElement('style')
  // style.appendChild(document.createTextNode('.msn-header .content-E1_1::before { content: none }'))
  // content.prepend(style)
  // const weatherContainer = document.querySelector('[class^="weatherContainer"]')
  // weatherContainer.style.margin = 0
  // document.querySelector('[class^="summaryCaption"]').remove()
  document.querySelector('[class^="summaryCaptionAndFeelLikeContainerMobile"]').remove()
  // disable scroll
  var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

  function preventDefault(e) {
    e.preventDefault();
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  }

  // modern Chrome requires { passive: false } when adding event
  var supportsPassive = false;
  try {
    window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
      get: function () { supportsPassive = true; }
    }));
  } catch (e) { }

  var wheelOpt = supportsPassive ? { passive: false } : false;
  var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

  // call this to Disable scroll
  function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
  }
  // call this to enable scroll
  function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
  }
  // reduce size only run if 2x2
  const media4x2 = matchMedia('(orientation: landscape)')
  if (media4x2.matches) {
    document.querySelector('[class^="detailContainerCompact"]')?.remove()
    document.querySelector('[class^="summaryContainerMobile"]').style.marginTop = '1rem'
    document.querySelector('[id^="WeatherForecast-ScreenWidth"]')?.remove()
    document.querySelector('[id^="WeatherMapMini-ScreenWidth"]')?.remove()
    document.querySelector('[id^="WeatherPollenTeaser"]')?.remove()
    disableScroll()
  }
  const media = window.matchMedia('(max-width: 279px)')
  if (media.matches) {
    // const labelWeather = document.querySelector('[class^="labelWeather"]')
    // labelWeather.style.fontSize = '12px'
    // const main = document.querySelector('#OverviewCurrentTemperature')
    // const icon = main.children[0]
    // icon.style.width = '48px'
    // const text = main.children[1]
    // text.style.fontSize = '40px'
    // const summaryContainer = document.querySelector('[class^="summaryContainerCompact"]')
    // summaryContainer.style.paddingLeft = '4px'
    document.querySelector('[class^="detailContainerCompact"]')?.remove()
    const weatherSummary = document.querySelector('[class^="summaryContainerMobile"]')
    weatherSummary.style.paddingLeft = '8px'
    weatherSummary.style.marginTop = '16px'
    const summary = document.querySelector('[class^="summaryTemperatureMobile"]')
    summary.style.marginLeft = '8px'
    summary.style.fontSize = '36px'
    summary.querySelector(':scope > span').fontSize = '16px'
    summary.querySelector(':scope > span').style.fontSize = '16px'
    disableScroll()
  }
}